﻿#pragma checksum "..\..\MainWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "DB5EBF7B90127B583998C13ECA8B980DDCC6AF32"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using GlobalTextil;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GlobalTextil {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 319 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel medicationStack;
        
        #line default
        #line hidden
        
        
        #line 321 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonOpenList;
        
        #line default
        #line hidden
        
        
        #line 322 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonOpenArticles;
        
        #line default
        #line hidden
        
        
        #line 323 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonOpenInfo;
        
        #line default
        #line hidden
        
        
        #line 324 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonOpenLogin;
        
        #line default
        #line hidden
        
        
        #line 325 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonLogout;
        
        #line default
        #line hidden
        
        
        #line 326 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonBasket;
        
        #line default
        #line hidden
        
        
        #line 327 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxSearch;
        
        #line default
        #line hidden
        
        
        #line 328 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonSearch;
        
        #line default
        #line hidden
        
        
        #line 336 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid basketGrid;
        
        #line default
        #line hidden
        
        
        #line 337 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel itemStackBasket;
        
        #line default
        #line hidden
        
        
        #line 338 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonBuy;
        
        #line default
        #line hidden
        
        
        #line 339 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonClearBasket;
        
        #line default
        #line hidden
        
        
        #line 341 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label priceBasket;
        
        #line default
        #line hidden
        
        
        #line 342 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel countStackBasket;
        
        #line default
        #line hidden
        
        
        #line 343 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel priceStackBasket;
        
        #line default
        #line hidden
        
        
        #line 345 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid orderListGrid;
        
        #line default
        #line hidden
        
        
        #line 346 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel orderList;
        
        #line default
        #line hidden
        
        
        #line 347 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel orderInfoList;
        
        #line default
        #line hidden
        
        
        #line 348 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonDeleteOrder;
        
        #line default
        #line hidden
        
        
        #line 350 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label priceOrder;
        
        #line default
        #line hidden
        
        
        #line 351 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label orderNumLabel;
        
        #line default
        #line hidden
        
        
        #line 352 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label orderStatusLabel;
        
        #line default
        #line hidden
        
        
        #line 354 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid searchGrid;
        
        #line default
        #line hidden
        
        
        #line 356 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stackList;
        
        #line default
        #line hidden
        
        
        #line 361 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid itemGrid;
        
        #line default
        #line hidden
        
        
        #line 362 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border imageItem;
        
        #line default
        #line hidden
        
        
        #line 366 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imageBox;
        
        #line default
        #line hidden
        
        
        #line 368 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label nameItemLabel;
        
        #line default
        #line hidden
        
        
        #line 370 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label priceItemLabel;
        
        #line default
        #line hidden
        
        
        #line 371 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label typeItemLabel;
        
        #line default
        #line hidden
        
        
        #line 374 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label sizeLabel;
        
        #line default
        #line hidden
        
        
        #line 375 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAddBasket;
        
        #line default
        #line hidden
        
        
        #line 376 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonItemBack;
        
        #line default
        #line hidden
        
        
        #line 377 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelAdded;
        
        #line default
        #line hidden
        
        
        #line 378 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock noteItemLabel;
        
        #line default
        #line hidden
        
        
        #line 380 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid contractGrid;
        
        #line default
        #line hidden
        
        
        #line 382 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox carBox;
        
        #line default
        #line hidden
        
        
        #line 384 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox addresBox;
        
        #line default
        #line hidden
        
        
        #line 386 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAddContract;
        
        #line default
        #line hidden
        
        
        #line 387 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateContractPicker;
        
        #line default
        #line hidden
        
        
        #line 395 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonCloseContract;
        
        #line default
        #line hidden
        
        
        #line 397 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid registerGrid;
        
        #line default
        #line hidden
        
        
        #line 398 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lNameBox;
        
        #line default
        #line hidden
        
        
        #line 399 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 400 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fNameBox;
        
        #line default
        #line hidden
        
        
        #line 402 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox pNameBox;
        
        #line default
        #line hidden
        
        
        #line 404 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox loginBox;
        
        #line default
        #line hidden
        
        
        #line 406 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox passwordBox;
        
        #line default
        #line hidden
        
        
        #line 408 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox repeatPasswordBox;
        
        #line default
        #line hidden
        
        
        #line 410 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox emailBox;
        
        #line default
        #line hidden
        
        
        #line 412 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonRegister;
        
        #line default
        #line hidden
        
        
        #line 413 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonCloseReg;
        
        #line default
        #line hidden
        
        
        #line 414 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox phoneBox;
        
        #line default
        #line hidden
        
        
        #line 417 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid loginGrid;
        
        #line default
        #line hidden
        
        
        #line 418 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox loginBoxLog;
        
        #line default
        #line hidden
        
        
        #line 420 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox passwordBoxLog;
        
        #line default
        #line hidden
        
        
        #line 422 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonLogIn;
        
        #line default
        #line hidden
        
        
        #line 423 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonRegistration;
        
        #line default
        #line hidden
        
        
        #line 425 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonCloseLogIn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GlobalTextil;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.medicationStack = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.buttonOpenList = ((System.Windows.Controls.Button)(target));
            
            #line 321 "..\..\MainWindow.xaml"
            this.buttonOpenList.Click += new System.Windows.RoutedEventHandler(this.OpenItemListButCl);
            
            #line default
            #line hidden
            return;
            case 3:
            this.buttonOpenArticles = ((System.Windows.Controls.Button)(target));
            
            #line 322 "..\..\MainWindow.xaml"
            this.buttonOpenArticles.Click += new System.Windows.RoutedEventHandler(this.OpenOrdersMenu);
            
            #line default
            #line hidden
            return;
            case 4:
            this.buttonOpenInfo = ((System.Windows.Controls.Button)(target));
            
            #line 323 "..\..\MainWindow.xaml"
            this.buttonOpenInfo.Click += new System.Windows.RoutedEventHandler(this.OpenLoginMenu);
            
            #line default
            #line hidden
            return;
            case 5:
            this.buttonOpenLogin = ((System.Windows.Controls.Button)(target));
            
            #line 324 "..\..\MainWindow.xaml"
            this.buttonOpenLogin.Click += new System.Windows.RoutedEventHandler(this.OpenLoginMenu);
            
            #line default
            #line hidden
            return;
            case 6:
            this.buttonLogout = ((System.Windows.Controls.Button)(target));
            
            #line 325 "..\..\MainWindow.xaml"
            this.buttonLogout.Click += new System.Windows.RoutedEventHandler(this.LogOut);
            
            #line default
            #line hidden
            return;
            case 7:
            this.buttonBasket = ((System.Windows.Controls.Button)(target));
            
            #line 326 "..\..\MainWindow.xaml"
            this.buttonBasket.Click += new System.Windows.RoutedEventHandler(this.OpenBasket);
            
            #line default
            #line hidden
            return;
            case 8:
            this.textBoxSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 327 "..\..\MainWindow.xaml"
            this.textBoxSearch.KeyDown += new System.Windows.Input.KeyEventHandler(this.SearchKey);
            
            #line default
            #line hidden
            return;
            case 9:
            this.buttonSearch = ((System.Windows.Controls.Button)(target));
            
            #line 328 "..\..\MainWindow.xaml"
            this.buttonSearch.Click += new System.Windows.RoutedEventHandler(this.SearchItemButCl);
            
            #line default
            #line hidden
            return;
            case 10:
            this.basketGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 11:
            this.itemStackBasket = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 12:
            this.buttonBuy = ((System.Windows.Controls.Button)(target));
            
            #line 338 "..\..\MainWindow.xaml"
            this.buttonBuy.Click += new System.Windows.RoutedEventHandler(this.OpenContractGrid);
            
            #line default
            #line hidden
            return;
            case 13:
            this.buttonClearBasket = ((System.Windows.Controls.Button)(target));
            
            #line 339 "..\..\MainWindow.xaml"
            this.buttonClearBasket.Click += new System.Windows.RoutedEventHandler(this.ClearBasketButCl);
            
            #line default
            #line hidden
            return;
            case 14:
            this.priceBasket = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.countStackBasket = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 16:
            this.priceStackBasket = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 17:
            this.orderListGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 18:
            this.orderList = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 19:
            this.orderInfoList = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 20:
            this.buttonDeleteOrder = ((System.Windows.Controls.Button)(target));
            
            #line 348 "..\..\MainWindow.xaml"
            this.buttonDeleteOrder.Click += new System.Windows.RoutedEventHandler(this.DeleteOrder);
            
            #line default
            #line hidden
            return;
            case 21:
            this.priceOrder = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.orderNumLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 23:
            this.orderStatusLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 24:
            this.searchGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 25:
            this.stackList = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 26:
            this.itemGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 27:
            this.imageItem = ((System.Windows.Controls.Border)(target));
            return;
            case 28:
            this.imageBox = ((System.Windows.Controls.Image)(target));
            return;
            case 29:
            this.nameItemLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 30:
            this.priceItemLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 31:
            this.typeItemLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 32:
            this.sizeLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 33:
            this.buttonAddBasket = ((System.Windows.Controls.Button)(target));
            
            #line 375 "..\..\MainWindow.xaml"
            this.buttonAddBasket.Click += new System.Windows.RoutedEventHandler(this.AddInBasket);
            
            #line default
            #line hidden
            return;
            case 34:
            this.buttonItemBack = ((System.Windows.Controls.Button)(target));
            
            #line 376 "..\..\MainWindow.xaml"
            this.buttonItemBack.Click += new System.Windows.RoutedEventHandler(this.CloseThisGrid);
            
            #line default
            #line hidden
            return;
            case 35:
            this.labelAdded = ((System.Windows.Controls.Label)(target));
            return;
            case 36:
            this.noteItemLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 37:
            this.contractGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 38:
            this.carBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 39:
            this.addresBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 40:
            this.buttonAddContract = ((System.Windows.Controls.Button)(target));
            
            #line 386 "..\..\MainWindow.xaml"
            this.buttonAddContract.Click += new System.Windows.RoutedEventHandler(this.Buy);
            
            #line default
            #line hidden
            return;
            case 41:
            this.dateContractPicker = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 42:
            this.buttonCloseContract = ((System.Windows.Controls.Button)(target));
            
            #line 395 "..\..\MainWindow.xaml"
            this.buttonCloseContract.Click += new System.Windows.RoutedEventHandler(this.CloseThisGrid);
            
            #line default
            #line hidden
            return;
            case 43:
            this.registerGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 44:
            this.lNameBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 45:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 46:
            this.fNameBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 47:
            this.pNameBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 48:
            this.loginBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 49:
            this.passwordBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 50:
            this.repeatPasswordBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 51:
            this.emailBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 52:
            this.buttonRegister = ((System.Windows.Controls.Button)(target));
            
            #line 412 "..\..\MainWindow.xaml"
            this.buttonRegister.Click += new System.Windows.RoutedEventHandler(this.Register);
            
            #line default
            #line hidden
            return;
            case 53:
            this.buttonCloseReg = ((System.Windows.Controls.Button)(target));
            
            #line 413 "..\..\MainWindow.xaml"
            this.buttonCloseReg.Click += new System.Windows.RoutedEventHandler(this.CloseThisGrid);
            
            #line default
            #line hidden
            return;
            case 54:
            this.phoneBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 55:
            this.loginGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 56:
            this.loginBoxLog = ((System.Windows.Controls.TextBox)(target));
            return;
            case 57:
            this.passwordBoxLog = ((System.Windows.Controls.TextBox)(target));
            return;
            case 58:
            this.buttonLogIn = ((System.Windows.Controls.Button)(target));
            
            #line 422 "..\..\MainWindow.xaml"
            this.buttonLogIn.Click += new System.Windows.RoutedEventHandler(this.LogIn);
            
            #line default
            #line hidden
            return;
            case 59:
            this.buttonRegistration = ((System.Windows.Controls.Button)(target));
            
            #line 423 "..\..\MainWindow.xaml"
            this.buttonRegistration.Click += new System.Windows.RoutedEventHandler(this.OpenRegisterMenu);
            
            #line default
            #line hidden
            return;
            case 60:
            this.buttonCloseLogIn = ((System.Windows.Controls.Button)(target));
            
            #line 425 "..\..\MainWindow.xaml"
            this.buttonCloseLogIn.Click += new System.Windows.RoutedEventHandler(this.CloseThisGrid);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

