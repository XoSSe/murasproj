﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GlobalTextil
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int price;                  // Общая сумма за лекарства в корзине
        int myID;                   // Уникальный идентификатор аккаунта

        bool stateLog;              // Состояние авторизации (авторизирован/неавторизирован)

        List<int> listAddedItems;   // Список уникальных идентификаторов лекарств в корзине

        public MainWindow()
        {
            InitializeComponent();

            price = 0;
            listAddedItems = new List<int>();
            stateLog = false;

            CloseSmallGrids();
            CloseAllGrids();
            OpenItemList();
        }

        /// <summary>
        /// Метод закрытия всех окон
        /// </summary>
        private void CloseAllGrids()
        {
            loginGrid.Visibility = Visibility.Hidden;
            registerGrid.Visibility = Visibility.Hidden;
            contractGrid.Visibility = Visibility.Hidden;
            basketGrid.Visibility = Visibility.Hidden;
            itemGrid.Visibility = Visibility.Hidden;
            orderListGrid.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Метод закрытия поверхностных окошек
        /// </summary>
        private void CloseSmallGrids()
        {
            loginGrid.Visibility = Visibility.Hidden;
            registerGrid.Visibility = Visibility.Hidden;
            contractGrid.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Метод регистрации
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void Register(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lNameBox.Text == "" || fNameBox.Text == "" || loginBox.Text == "" || passwordBox.Text == "" ||
                    repeatPasswordBox.Text == "" || emailBox.Text == "")
                {
                    MessageBox.Show("Не все поля заполнены!");
                }
                else
                {
                    string[] arrParm = { "@loginIn", "@resultOut" };
                    string[] arrParmData = { loginBox.Text };
                    string[] arrParmOut = MySqlClass.executeProcedureInOutItem("CheckAcc", arrParm, arrParmData);

                    if (arrParmOut[0] == "0")
                    {
                        string[] arrParmAcc = { "@loginIn", "@passwordIn", "@emailIn" };
                        string[] arrParmDataAcc = { loginBox.Text, passwordBox.Text, emailBox.Text };
                        MySqlClass.executeProcedureInItem("RegisterAcc", arrParmAcc, arrParmDataAcc);

                        string[] arrParmMyId = { "@loginIn", "@IDOut" };
                        string[] arrParmDataMyId = { loginBox.Text };
                        string[] arrParmOutMyId = MySqlClass.executeProcedureInOutItem("GetAccountID", arrParmMyId, arrParmDataMyId);

                        string[] arrParmAccCL = { "@lNameIn", "@fNameIn", "@pNameIn", "@idAccIN", "@numbIn" };
                        string[] arrParmDataAccCL = { lNameBox.Text, fNameBox.Text, pNameBox.Text, arrParmOutMyId[0], phoneBox.Text };
                        MySqlClass.executeProcedureInItem("RegisterClient", arrParmAccCL, arrParmDataAccCL);

                        CloseAllGrids();
                        loginGrid.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        MessageBox.Show("Такой аккаунт уже есть!");
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Метод авторизации
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void LogIn(object sender, RoutedEventArgs e)
        {
            try
            {
                if (loginBoxLog.Text == "" || passwordBoxLog.Text == "")
                {
                    MessageBox.Show("Не все поля заполнены!");
                }
                else
                {
                    string[] arrParm = { "@loginIn", "@passIn", "@loginOut" };
                    string[] arrParmData = { loginBoxLog.Text, passwordBoxLog.Text };
                    string[] arrParmOut = MySqlClass.executeProcedureInOutItem("CheckAccLogIn", arrParm, arrParmData);

                    if (arrParmOut[0] == "1")
                    {
                        CloseAllGrids();
                        OpenItemList();
                        buttonLogout.Visibility = Visibility.Visible;
                        buttonLogIn.Visibility = Visibility.Hidden;
                        medicationStack.Visibility = Visibility.Visible;

                        stateLog = true;

                        FillItemStack();
                    }
                    else
                    {
                        MessageBox.Show("Неверный логин или пароль!");
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Метод заполнения данных
        /// </summary>
        private void FillItemStack()
        {
            medicationStack.Children.Clear();

            string[] arrParm = { "@idItemOUT", "@itemsOUT" };
            string[] arrParmData = { };
            string[] arrParmOut = MySqlClass.executeProcedureInOutItem("GetAllItems", arrParm, arrParmData);

            string[] itemsIdArr = arrParmOut[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] itemsArr = arrParmOut[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            string[] arrParmMyId = { "@loginIn", "@passIn", "@IDOut" };
            string[] arrParmDataMyId = { loginBoxLog.Text, passwordBoxLog.Text };
            string[] arrParmOutMyId = MySqlClass.executeProcedureInOutItem("GetClientID", arrParmMyId, arrParmDataMyId);

            myID = Convert.ToInt32(arrParmOutMyId[0]);

            for (int i = 0; i < 4; ++i)
            {
                Button item = new Button();
                item.Content = itemsArr[i];
                item.Width = medicationStack.Width - 25;
                item.Height = 100;
                item.FontSize = 20;
                item.FontWeight = FontWeights.SemiBold;
                item.Style = Resources["ButtonRight"] as Style;
                item.Uid = itemsIdArr[i];
                item.Click += new RoutedEventHandler(ShowItemInfoClick);

                medicationStack.Children.Add(item);
            }
        }

        /// <summary>
        /// Метод открытия окошка информации о лекарстве
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void ShowItemInfoClick(object sender, RoutedEventArgs e)
        {
            ShowItemInfo(((Button)sender).Uid);
        }

        private void ShowItemInfo(string Id)
        {
            CloseSmallGrids();
            itemGrid.Visibility = Visibility.Visible;

            string[] arrParm = { "@itemIdIN", "@itemNameOUT", "@priceItemOUT", "@nameTypeItemOUT",
                "@widthItemOUT", "@lengthItemOUT", "@noteOUT", "@photoOUT" };
            string[] arrParmData = { Id };
            string[] arrParmOut = MySqlClass.executeProcedureInOutItem("GetItemInfoID", arrParm, arrParmData);

            nameItemLabel.Content = arrParmOut[0];
            priceItemLabel.Content = arrParmOut[1] + " ₽";
            typeItemLabel.Content = arrParmOut[2];
            sizeLabel.Content = arrParmOut[3] + "x" + arrParmOut[4] + " см";
            noteItemLabel.Text = arrParmOut[5];

            buttonAddBasket.Uid = Id;
            buttonAddBasket.Tag = Id + "/" + arrParmOut[0] + "/" + arrParmOut[1];

            imageBox.Source = GetImage(arrParmOut[6]);

            bool stateAdded = false;
            for (int i = 0; i < listAddedItems.Count; ++i)
            {
                if (Convert.ToInt32(Id) == listAddedItems[i])
                {
                    stateAdded = true;
                    break;
                }
            }

            if (stateAdded)
            {
                buttonAddBasket.Visibility = Visibility.Hidden;
                labelAdded.Visibility = Visibility.Visible;
            }
            else
            {
                buttonAddBasket.Visibility = Visibility.Visible;
                labelAdded.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Метод закрытия текущего окошка
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void CloseThisGrid(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();
            ((Grid)((Button)sender).Parent).Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Метод открытия окошка регистрации
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void OpenRegisterMenu(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();

            registerGrid.Visibility = Visibility.Visible;
            loginGrid.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Метод открытия окошка авторизации
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void OpenLoginMenu(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();

            loginGrid.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Метод выхода из аккаунта
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void LogOut(object sender, RoutedEventArgs e)
        {
            CloseAllGrids();
            OpenItemList();

            buttonLogout.Visibility = Visibility.Hidden;
            buttonLogIn.Visibility = Visibility.Visible;
            medicationStack.Visibility = Visibility.Hidden;
            stateLog = false;
        }

        /// <summary>
        /// Метод установки статуса кнопки оформления заказа
        /// </summary>
        private void SetButtonBuyState()
        {
            // Если корзина пустая, то кнопка неактивна
            if (itemStackBasket.Children.Count > 0)
                buttonBuy.IsEnabled = true;
            // Иначе активна
            else
                buttonBuy.IsEnabled = false;
        }

        /// <summary>
        /// Метод открытия окна корзины
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void OpenBasket(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();

            if (stateLog)
            {
                CloseAllGrids();
                searchGrid.Visibility = Visibility.Hidden;
                basketGrid.Visibility = Visibility.Visible;

                SetButtonBuyState();
            }
            else
            {
                loginGrid.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Метод добавления лекарства в корзину
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void AddInBasket(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();

            if (stateLog)
            {
                string[] info = (buttonAddBasket.Tag.ToString()).Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                bool state = false;

                for (int i = 0; i < itemStackBasket.Children.Count; ++i)
                {
                    if (info[0] == ((Button)(itemStackBasket.Children[i])).Uid.ToString())
                    {
                        state = true;

                        break;
                    }
                }

                if (!state)
                {
                    buttonAddBasket.Visibility = Visibility.Hidden;
                    labelAdded.Visibility = Visibility.Visible;

                    listAddedItems.Add(Convert.ToInt32(info[0]));

                    Button item = new Button();

                    item.Content = info[1];
                    item.Width = itemStackBasket.Width - 25;
                    item.Height = 25;
                    item.FontWeight = FontWeights.SemiBold;
                    item.Style = Resources["ButtonOnGr"] as Style;
                    item.Click += new RoutedEventHandler(ShowItemInfoClick);
                    item.Uid = info[0];
                    item.Tag = (itemStackBasket.Children.Count - 1).ToString();

                    ComboBox itemCount = new ComboBox();

                    itemCount.Width = countStackBasket.Width;
                    itemCount.Height = 25;
                    itemCount.FontWeight = FontWeights.SemiBold;
                    itemCount.Style = Resources["ComboBoxFlatStyle"] as Style;

                    for (int i = 1; i <= 50; ++i)
                        itemCount.Items.Add(i);

                    itemCount.SelectedIndex = 0;

                    Label itemPrice = new Label();

                    itemPrice.Content = info[2] + " ₽";
                    itemPrice.Width = priceStackBasket.Width;
                    itemPrice.Height = 25;
                    itemPrice.FontWeight = FontWeights.SemiBold;
                    itemPrice.Uid = (itemStackBasket.Children.Count - 1).ToString();
                    itemPrice.Tag = info[2];
                    itemPrice.Foreground = System.Windows.Media.Brushes.White;
                    itemPrice.HorizontalContentAlignment = HorizontalAlignment.Right;

                    itemStackBasket.Children.Add(item);
                    countStackBasket.Children.Add(itemCount);
                    priceStackBasket.Children.Add(itemPrice);

                    itemCount.Uid = (itemStackBasket.Children.Count - 1).ToString();
                    itemCount.SelectionChanged += new SelectionChangedEventHandler(ChangePriceItem);

                    price += Convert.ToInt32(info[2]);
                    priceBasket.Content = price + " ₽";
                }
            }
            else
            {
                loginGrid.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Метод отправки заказа в БД
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void Buy(object sender, RoutedEventArgs e)
        {
            try
            {
                if (itemStackBasket.Children.Count > 0)
                {
                    if (addresBox.Text != "" && dateContractPicker.SelectedDate != null)
                    {
                        CloseSmallGrids();

                        DateTime? date = dateContractPicker.SelectedDate;
                        string year = date.Value.Date.Year.ToString() + ".";
                        string month = date.Value.Date.Month.ToString() + ".";
                        string day = date.Value.Date.Day.ToString();

                        string[] regNum = (carBox.SelectedValue.ToString()).Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

                        string[] arrParmCar = { "@regNumIN", "@idCarOUT" };
                        string[] arrParmDataCar = { regNum[1] };
                        string[] arrParmOutCar = MySqlClass.executeProcedureInOutItem("GetCarID", arrParmCar, arrParmDataCar);

                        string[] arrParm = { "@idClientIN", "@idCarIN", "@addresContractIN", "@dateContractIN" };
                        string[] arrParmData = { myID.ToString(), arrParmOutCar[0], addresBox.Text, year + month + day };
                        MySqlClass.executeProcedureInItem("AddContract", arrParm, arrParmData);

                        string[] arrParmIdContr = { "@idContractOUT" };
                        string[] arrParmDataIdContr = { };
                        string[] arrParmOutIdContr = MySqlClass.executeProcedureInOutItem("GetLastContractID", arrParmIdContr, arrParmDataIdContr);
                        string idContract = arrParmOutIdContr[0];

                        for (int i = 0; i < itemStackBasket.Children.Count; ++i)
                        {
                            string[] arrParmACHI = { "@idContractIN", "@idItemIN", "@countIN" };
                            string[] arrParmDataACHI = { idContract, ((Button)(itemStackBasket.Children[i])).Uid.ToString(), ((ComboBox)(countStackBasket.Children[i])).SelectedValue.ToString() };
                            MySqlClass.executeProcedureInItem("AddContractHasItems", arrParmACHI, arrParmDataACHI);
                        }

                        ClearBasket();

                        contractGrid.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        MessageBox.Show("Не все поля заполнены!");
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Метод смены цены лекаства
        /// </summary>
        /// <param name="sender">Объект списка</param>
        /// <param name="e">Событие списка</param>
        private void ChangePriceItem(object sender, SelectionChangedEventArgs e)
        {
            CloseSmallGrids();

            int thisUid = Convert.ToInt32(((ComboBox)sender).Uid);

            ((Label)(priceStackBasket.Children[thisUid])).Content =
                Convert.ToInt32(((Label)(priceStackBasket.Children[thisUid])).Tag) *
                Convert.ToInt32(((ComboBox)sender).SelectedValue) + " ₽";

            price = 0;
            for (int i = 0; i < itemStackBasket.Children.Count; ++i)
            {
                price += Convert.ToInt32(((Label)(priceStackBasket.Children[i])).Tag) *
                    Convert.ToInt32(((ComboBox)(countStackBasket.Children[i])).SelectedValue);
            }
            priceBasket.Content = price + " ₽";
        }

        /// <summary>
        /// Метод нажатия на кнопку очистки корзины
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void ClearBasketButCl(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();
            ClearBasket();
        }

        /// <summary>
        /// Метод очистки корзины
        /// </summary>
        private void ClearBasket()
        {
            itemStackBasket.Children.Clear();
            countStackBasket.Children.Clear();
            priceStackBasket.Children.Clear();
            listAddedItems.Clear();

            price = 0;
            priceBasket.Content = price + " ₽";

            SetButtonBuyState();
        }

        /// <summary>
        /// Метод открытия окошка заполнения формы заказа
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void OpenContractGrid(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();

            if (stateLog)
            {
                dateContractPicker.DisplayDateStart = DateTime.Now;
                dateContractPicker.SelectedDate = DateTime.Now;
                contractGrid.Visibility = Visibility.Visible;

                carBox.Items.Clear();

                string[] arrParm = { "@carOUT" };
                string[] arrParmData = { };
                string[] arrParmOut = MySqlClass.executeProcedureInOutItem("GetAllCars", arrParm, arrParmData);
                string[] carArr = arrParmOut[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < carArr.Length; ++i)
                    carBox.Items.Add(carArr[i]);
            }
            else
            {
                loginGrid.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Метод получения изображения по URL
        /// </summary>
        /// <param name="imgStr">URL ссылка на изображение</param>
        /// <returns>Возвращаемое изображение</returns>
        private BitmapImage GetImage(string imgStr)
        {
            try
            {
                BitmapImage img = new BitmapImage();
                img.BeginInit();
                img.UriSource = new Uri(imgStr, UriKind.Absolute);
                img.EndInit();

                return img;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Метод нажатия кнопки поиска лекарств
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void SearchItemButCl(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();
            SearchItem();
        }

        /// <summary>
        /// Метод нажатия кнопки открытия списка лекарств
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие кнопки</param>
        private void OpenItemListButCl(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();
            OpenItemList();
        }

        /// <summary>
        /// Метод открытия списка лекарств
        /// </summary>
        private void OpenItemList()
        {
            CloseAllGrids();
            searchGrid.Visibility = Visibility.Visible;

            stackList.Children.Clear();

            string[] arrParm = { "@idItemOUT", "@itemsOUT" };
            string[] arrParmData = { };
            string[] arrParmOut = MySqlClass.executeProcedureInOutItem("GetAllItems", arrParm, arrParmData);

            string[] itemsIdArr = arrParmOut[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] itemsArr = arrParmOut[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            FillItemList(itemsArr, itemsIdArr);
        }

        /// <summary>
        /// Метод поиска
        /// </summary>
        private void SearchItem()
        {
            CloseAllGrids();
            searchGrid.Visibility = Visibility.Visible;

            stackList.Children.Clear();

            string[] arrParm = { "@nameIN", "@idItemOUT", "@itemsOUT" };
            string[] arrParmData = { textBoxSearch.Text };
            string[] arrParmOut = MySqlClass.executeProcedureInOutItem("GetAllItemsOnName", arrParm, arrParmData);

            string[] itemsIdArr = arrParmOut[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] itemsArr = arrParmOut[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            
            FillItemList(itemsArr, itemsIdArr);
        }

        /// <summary>
        /// Метод заполнения списка лекарств
        /// </summary>
        /// <param name="count">Количество элементов массива</param>
        /// <param name="medList">Массив уникальных идентификаторов лекарств</param>
        private void FillItemList(string[] medList, string[] medListIds)
        {
            for (int i = 0; i < medList.Length; ++i)
            {
                string[] arrParmInfo = { "@itemIdIN", "@itemNameOUT", "@nameProducerOUT", "@priceItemOUT", "@nameTypeItemOUT",
                "@widthItemOUT", "@lengthItemOUT", "@noteOUT", "@photoOUT" };
                string[] arrParmDataInfo = { medListIds[i] };
                string[] arrParmOutInfo = MySqlClass.executeProcedureInOutItem("GetItemInfoID", arrParmInfo, arrParmDataInfo);
                
                Border brdr = new Border();
                brdr.BorderBrush = System.Windows.Media.Brushes.Black;
                brdr.BorderThickness = new Thickness(1, 1, 1, 1);
                
                StackPanel panel = new StackPanel();
                panel.Height = 100;
                panel.Background = System.Windows.Media.Brushes.White;
                panel.Orientation = Orientation.Horizontal;
                
                System.Windows.Controls.Image img = new System.Windows.Controls.Image();
                img.Width = 100;
                img.Height = 100;
                img.Source = GetImage(arrParmOutInfo[7]);
                
                TextBlock nameLab = new TextBlock();
                nameLab.Width = 150;
                nameLab.Height = 100;
                nameLab.FontSize = 20;
                nameLab.FontWeight = FontWeights.SemiBold;
                nameLab.Text = arrParmOutInfo[0];
                nameLab.TextWrapping = TextWrapping.Wrap;
                nameLab.Foreground = System.Windows.Media.Brushes.Black;
                
                Button but = new Button();
                but.Content = arrParmOutInfo[0];
                but.Width = itemStackBasket.Width - 25;
                but.Height = 100;
                but.Style = Resources["ButtonRight"] as Style;
                but.FontWeight = FontWeights.SemiBold;
                but.Click += new RoutedEventHandler(ShowItemInfoClick);
                but.Uid = medListIds[i];
                
                panel.Children.Add(img);
                panel.Children.Add(nameLab);
                panel.Children.Add(but);
                brdr.Child = panel;
                stackList.Children.Add(brdr);
            }
        }

        /// <summary>
        /// Метод выполнения поиска при нажатии клавиши
        /// </summary>
        /// <param name="sender">Объект поля поиска</param>
        /// <param name="e">Событие нажатия клавиши</param>
        private void SearchKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                SearchItem();
        }

        /// <summary>
        /// Метод открытия панели заказов пользователя
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие нажатия</param>
        private void OpenOrdersMenu(object sender, RoutedEventArgs e)
        {
            CloseSmallGrids();

            if (stateLog)
            {
                CloseAllGrids();
                searchGrid.Visibility = Visibility.Hidden;
                orderListGrid.Visibility = Visibility.Visible;

                FillOrderList();
            }
            else
            {
                loginGrid.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Метод вывода списка заказов пользователя
        /// </summary>
        private void FillOrderList()
        {
            orderList.Children.Clear();
            orderInfoList.Children.Clear();
            priceOrder.Content = "0 ₽";
            orderNumLabel.Content = "Заказ";
            orderStatusLabel.Foreground = System.Windows.Media.Brushes.Black;
            orderStatusLabel.Content = "";
            buttonDeleteOrder.IsEnabled = false;

            string[] arrParmInfo = { "@statusContractIn", "@clientIdIn", "@contractIdOut", "@carIdOut", "@addrOut", "@dateContr" };
            string[] arrParmDataInfo = { "0", myID.ToString() };
            string[] arrParmOutInfo = MySqlClass.executeProcedureInOutItem("GetContractOnStatus", arrParmInfo, arrParmDataInfo);

            string[] arrOrders = arrParmOutInfo[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < arrOrders.Length; ++i)
            {
                Button orderBut = new Button();
                orderBut.Content = "Заказ №" + i;
                orderBut.Tag = arrOrders[i];
                orderBut.Uid = i.ToString();
                orderBut.Click += FillOrderInfo;
                orderBut.Style = Resources["ButtonOnGr"] as Style;

                orderList.Children.Add(orderBut);
            }
        }

        /// <summary>
        /// Метод заполнения информацией о заказе пользователя
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие нажатия</param>
        private void FillOrderInfo(object sender, RoutedEventArgs e)
        {
            orderInfoList.Children.Clear();

            string[] arrParmInfo = { "@idContractIN", "@itemIdOUT", "@itemNameOUT", "@countOUT", "@priceOUT", "@statusOUT" };
            string[] arrParmDataInfo = { ((Button)sender).Tag.ToString() };
            string[] arrParmOutInfo = MySqlClass.executeProcedureInOutItem("GetOrderItems", arrParmInfo, arrParmDataInfo);

            string[] arrItemsId = arrParmOutInfo[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] arrItemsName = arrParmOutInfo[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] arrItemsCount = arrParmOutInfo[2].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < arrItemsName.Length; ++i)
            {
                StackPanel panel = new StackPanel();
                //panel.Height = 100;
                //panel.Background = System.Windows.Media.Brushes.White;
                panel.Orientation = Orientation.Horizontal;

                Button orderBut = new Button();
                orderBut.Content = arrItemsName[i];
                orderBut.Width = 200;
                orderBut.Uid = arrItemsId[i];
                orderBut.Click += new RoutedEventHandler(ShowItemInfoClick);
                orderBut.Style = Resources["ButtonOnGr"] as Style;

                Label countLabel = new Label();
                countLabel.Content = "×" + arrItemsCount[i];
                //countLabel.Width = priceStackBasket.Width;
                //countLabel.Height = 25;
                countLabel.FontWeight = FontWeights.SemiBold;
                countLabel.Foreground = System.Windows.Media.Brushes.White;
                countLabel.HorizontalContentAlignment = HorizontalAlignment.Left;

                panel.Children.Add(orderBut);
                panel.Children.Add(countLabel);
                orderInfoList.Children.Add(panel);
            }

            orderNumLabel.Content = "Заказ №" + ((Button)sender).Uid.ToString();
            priceOrder.Content = arrParmOutInfo[3] + " ₽";

            if (arrParmOutInfo[4] == "0")
            {
                orderStatusLabel.Foreground = System.Windows.Media.Brushes.Red;
                orderStatusLabel.Content = "В обработке";
                buttonDeleteOrder.IsEnabled = true;
            }
            else
            {
                orderStatusLabel.Foreground = System.Windows.Media.Brushes.Green;
                orderStatusLabel.Content = "Отправлен";
                buttonDeleteOrder.IsEnabled = false;
            }

            buttonDeleteOrder.Uid = ((Button)sender).Tag.ToString();
        }

        /// <summary>
        /// Метод удаления заказа пользователя
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие нажатия</param>
        private void DeleteOrder(object sender, RoutedEventArgs e)
        {
            string[] arrParmInfo = { "@contractID" };
            string[] arrParmDataInfo = { ((Button)sender).Uid.ToString() };
            string[] arrParmOutInfo = MySqlClass.executeProcedureInOutItem("RemoveContract", arrParmInfo, arrParmDataInfo);

            FillOrderList();
        }
    }
}
