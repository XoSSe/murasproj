﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Windows;
using MySql.Data.MySqlClient;

namespace GlobalTextil
{
    class MySqlClass
    {
        private static string sConnString = @"server = 81.200.119.82;
            database = Tekstil;
            charset = utf8;
            user id = student;
            port = 3306;
            password = Student!@#;
            Allow Zero Datetime = True;"; //Строка подключения к серверу

        private static MySqlConnection mySQLConn = new MySqlConnection(sConnString); // Объект подключения к БД

        /// <summary>
        /// Метод открытия соединения с БД
        /// </summary>
        private static void OpenConnect()
        {
            try
            {
                mySQLConn.Open(); //Открытие соединения
            }
            catch
            {
                MessageBox.Show("Соединение с сервером отсутствует"); // Информационное окно в случае возникновения ошибки
            }
        }

        /// <summary>
        /// Формирования объекта команды БД для работы с БД через хранимую процедуру
        /// </summary>
        /// <param name="nameProcedure">Наименование процедуры</param>
        /// <returns>Объект команды БД</returns>
        private static MySqlCommand GetCommandProcedure(string nameProcedure)
        {
            MySqlCommand cmd = new MySqlCommand(); //Создание новго объекта в виде команды MySQL

            cmd.Connection = mySQLConn; // Установления ассоциации для команды соединения
            cmd.CommandType = CommandType.StoredProcedure; // Обаначения команды как тип хранимая процедура
            cmd.CommandText = nameProcedure; //Передача наименования хранимой процедуры

            return cmd;
        }

        /// <summary>
        /// Формирования объекта команды БД для работы с БД через хранимую процедуру с входными параметрами
        /// </summary>
        /// <param name="nameProcedure">Наименование процедуры</param>
        /// <param name="arrParam">Массив с наименованием параметров</param>
        /// <param name="arrParamData">Массив с данными</param>
        /// <returns>Объект команды БД</returns>
        private static MySqlCommand GetCommandProcedureInParam(string nameProcedure, string[] arrParam, string[] arrParamData)
        {
            MySqlCommand cmd = GetCommandProcedure(nameProcedure);

            for (int i = 0; i < arrParam.Length; i++)
            {
                cmd.Parameters.Add(new MySqlParameter(arrParam[i], MySqlDbType.VarChar, 1000)); //Добавления наименования параметров в объект команды
                cmd.Parameters[arrParam[i]].Value = arrParamData[i];  // Добавление значения в параметр
            }

            return cmd;
        }

        /// <summary>
        /// Метод выполения процедуры в которой есть только входные параметры
        /// </summary>
        /// <param name="nameProcedure">Наименование процедуры</param>
        /// <param name="arrParam">Массив с наименованием параметров</param>
        /// <param name="arrParamData">Массив с данными</param>
        public static void ExecuteProcedureInItem(string nameProcedure, string[] arrParam, string[] arrParamData)
        {
            OpenConnect();

            GetCommandProcedureInParam(nameProcedure, arrParam, arrParamData).ExecuteNonQuery(); //Выполнение команды

            mySQLConn.Close(); // Закрытие соединения (Перемещается в пул)
        }

        /// <summary>
        /// Метод получения выходных параметров через процедру и входные параметры
        /// </summary>
        /// <param name="nameProcedure">Наименование процедуры</param>
        /// <param name="arrParam">Массив с наименованием параметров</param>
        /// <param name="arrParamData">Массив с данными</param>
        /// <returns>Выходные параметры</returns>
        public static string[] ExecuteProcedureInOutItem(string nameProcedure, string[] arrParam, string[] arrParamData)
        {
            OpenConnect();

            MySqlCommand cmd = GetCommandProcedure(nameProcedure);

            string[] arrParamOut = new string[arrParam.Length - arrParamData.Length]; //Массив выходных параметров

            for (int i = 0; i < arrParam.Length; i++)
            {
                cmd.Parameters.Add(new MySqlParameter(arrParam[i], MySqlDbType.VarChar, 1000)); //Добавления всех параметров в объект команды

                if (i < arrParamData.Length)
                    cmd.Parameters[arrParam[i]].Value = arrParamData[i]; //Занесения значений параметров в объект команды
                else
                    cmd.Parameters[arrParam[i]].Direction = ParameterDirection.Output; // Обозначения параметра как выходной
            }
            cmd.ExecuteNonQuery(); //Выполнение команды

            for (int i = 0; i < arrParamOut.Length; i++)
                arrParamOut[i] = Convert.ToString(cmd.Parameters[arrParam[arrParamData.Length + i]].Value); //Присвоение массиву выходных параметров полученных данных из процедуры

            mySQLConn.Close(); // Закрытие соединения (Перемещается в пул)

            return arrParamOut;
        }

        /// <summary>
        /// Метод получения таблицы без входных параметров
        /// </summary>
        /// <param name="nameProcedure">Наименование процедуры</param>
        /// <returns>Вывод объекта в формате DateView (работа с таблицами)</returns>
        public static DataView GetTableProcedure(string nameProcedure)
        {
            OpenConnect();

            DataTable DT = new DataTable(); // Создания специального объекта для работы с таблицами
            (new MySqlDataAdapter(GetCommandProcedure(nameProcedure))).Fill(DT); // Получения таблицы путе создания нового адаптера с командой cmd и транслирования её в объект DT
            mySQLConn.Close(); // Закрытие соединения (Перемещается в пул)

            return DT.AsDataView();
        }
    }
}
