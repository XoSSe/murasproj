-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 81.200.119.82    Database: Tekstil
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `СompletedOrders`
--

DROP TABLE IF EXISTS `СompletedOrders`;
/*!50001 DROP VIEW IF EXISTS `СompletedOrders`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `СompletedOrders` AS SELECT 
 1 AS `idContract`,
 1 AS `idClient`,
 1 AS `idCar`,
 1 AS `addresContract`,
 1 AS `dateContract`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Accounts`
--

DROP TABLE IF EXISTS `Accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Accounts` (
  `idAcc` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(32) NOT NULL,
  `idTypeAccount` int(11) NOT NULL,
  PRIMARY KEY (`idAcc`),
  UNIQUE KEY `password_UNIQUE` (`login`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `idTypeAccount_idx` (`idTypeAccount`),
  CONSTRAINT `idTypeAccount` FOREIGN KEY (`idTypeAccount`) REFERENCES `typeAccount` (`idTypeAccount`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Accounts`
--

LOCK TABLES `Accounts` WRITE;
/*!40000 ALTER TABLE `Accounts` DISABLE KEYS */;
INSERT INTO `Accounts` VALUES (1,'Vadimka','123321','vadim-molotov@mail.ru',1),(2,'kovalevss','qwe123','kovalev_sergei_p1-15@yandex.ru',1),(3,'admin','admin','molochko@gmail.com',2),(4,'t','t','t',1),(5,'AlexeyD','q2e2','alexeydolgov@mail.ru',1),(6,'Mirniy','ooo2135','bazzer@yandex.ru',1),(7,'MishaD','123321','miha@mail.ru',1);
/*!40000 ALTER TABLE `Accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cars`
--

DROP TABLE IF EXISTS `Cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cars` (
  `idCar` int(11) NOT NULL AUTO_INCREMENT,
  `carName` varchar(45) NOT NULL,
  `carModel` varchar(45) NOT NULL,
  `idWorker` int(11) NOT NULL,
  `regNum` varchar(10) NOT NULL,
  `widthBody` int(11) NOT NULL,
  `heightBody` int(11) NOT NULL,
  `lengthBody` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCar`),
  UNIQUE KEY `regNum_UNIQUE` (`regNum`),
  KEY `idWorker_idx` (`idWorker`),
  CONSTRAINT `idWorker` FOREIGN KEY (`idWorker`) REFERENCES `Workers` (`idWorker`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cars`
--

LOCK TABLES `Cars` WRITE;
/*!40000 ALTER TABLE `Cars` DISABLE KEYS */;
INSERT INTO `Cars` VALUES (1,'1','1',1,'1',1,1,1,1);
/*!40000 ALTER TABLE `Cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Clients`
--

DROP TABLE IF EXISTS `Clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Clients` (
  `idClient` int(11) NOT NULL AUTO_INCREMENT,
  `LNameC` varchar(45) NOT NULL,
  `FNameC` varchar(45) NOT NULL,
  `PNameC` varchar(45) DEFAULT NULL,
  `idAcc` int(11) NOT NULL,
  `PhoneC` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`idClient`),
  UNIQUE KEY `idAccount_UNIQUE` (`idAcc`),
  UNIQUE KEY `PhoneC_UNIQUE` (`PhoneC`),
  CONSTRAINT `idAcc` FOREIGN KEY (`idAcc`) REFERENCES `Accounts` (`idAcc`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Clients`
--

LOCK TABLES `Clients` WRITE;
/*!40000 ALTER TABLE `Clients` DISABLE KEYS */;
INSERT INTO `Clients` VALUES (1,'2','3','4',4,'4'),(2,'Ковалёв','Сергей','Сергеевич',2,'89106667423'),(3,'Долгов','Алексей','Денисович',5,'89175563432'),(4,'Мирный','Олег','Олегович',6,'89165988765'),(5,'Демидов','Михаил','Сергеевич',7,'');
/*!40000 ALTER TABLE `Clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contract`
--

DROP TABLE IF EXISTS `Contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contract` (
  `idContract` int(11) NOT NULL AUTO_INCREMENT,
  `idClient` int(11) NOT NULL,
  `idCar` int(11) NOT NULL,
  `addresContract` varchar(100) NOT NULL,
  `dateContract` date NOT NULL,
  `statusContract` tinyint(1) NOT NULL,
  PRIMARY KEY (`idContract`),
  KEY `idClient_idx` (`idClient`),
  KEY `idCar_idx` (`idCar`),
  CONSTRAINT `idCar` FOREIGN KEY (`idCar`) REFERENCES `Cars` (`idCar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idClient` FOREIGN KEY (`idClient`) REFERENCES `Clients` (`idClient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contract`
--

LOCK TABLES `Contract` WRITE;
/*!40000 ALTER TABLE `Contract` DISABLE KEYS */;
INSERT INTO `Contract` VALUES (2,1,1,'г. Москва, ул.Ленина, 15','2018-09-25',0),(3,1,1,'г. Москва, ул.Ленина, 11','2018-08-28',0),(4,1,1,'г. Москва, ул. Советская, 22','2018-08-22',1),(5,2,1,'г. Пушкино, ул. Пионерская, 12','2018-09-30',1),(6,2,1,'г. Пушкино, ул. Пионерская, 12','2018-10-15',0),(7,2,1,'г. Пушкино, ул. Пионерская, 12','2018-10-16',0),(8,3,1,'г.Москва, ул. Главная, 22','2018-10-15',0),(9,3,1,'г.Москва, ул. Главная, 22','2018-10-16',1),(10,3,1,'г.Москва, ул. Главная, 22','2018-10-30',1),(11,3,1,'г.Москва, ул. Ленина, 15','2018-12-01',0),(12,3,1,'г.Москва, ул. Ленина, 15','2018-12-02',0),(13,4,1,'г. Мытищи, ул. Вокзальная, 55','2018-10-15',1),(14,4,1,'г. Мытищи, ул. Вокзальная, 55','2018-10-16',1),(15,4,1,'г. Мытищи, ул. Вокзальная, 55','2018-10-30',0),(16,4,1,'г. Мытищи, ул. Вокзальная, 55','2018-12-01',0),(17,4,1,'г. Мытищи, ул. Вокзальная, 55','2018-12-02',0);
/*!40000 ALTER TABLE `Contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contract_has_Items`
--

DROP TABLE IF EXISTS `Contract_has_Items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contract_has_Items` (
  `idContract` int(11) NOT NULL,
  `idItem` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`idContract`,`idItem`),
  KEY `fk_Contract_has_Items_Contract1_idx` (`idContract`),
  KEY `idItem_idx` (`idItem`),
  CONSTRAINT `idContract` FOREIGN KEY (`idContract`) REFERENCES `Contract` (`idContract`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idItem` FOREIGN KEY (`idItem`) REFERENCES `Items` (`idItem`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contract_has_Items`
--

LOCK TABLES `Contract_has_Items` WRITE;
/*!40000 ALTER TABLE `Contract_has_Items` DISABLE KEYS */;
INSERT INTO `Contract_has_Items` VALUES (2,1,1),(2,4,4),(3,1,11),(3,2,16),(3,4,22),(4,1,10),(4,4,15),(5,2,1),(5,3,2),(5,4,5),(6,1,1),(7,2,3),(8,2,2),(9,3,5),(9,4,11),(10,2,3),(10,4,1),(11,1,1),(11,3,1),(12,2,1),(13,2,2),(13,4,2),(14,3,1),(15,2,1),(16,1,1),(16,2,3),(17,1,2),(17,4,4);
/*!40000 ALTER TABLE `Contract_has_Items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items`
--

DROP TABLE IF EXISTS `Items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items` (
  `idItem` int(11) NOT NULL AUTO_INCREMENT,
  `itemName` varchar(100) NOT NULL,
  `priceItem` int(11) NOT NULL,
  `idTypeItem` int(11) NOT NULL,
  `widthItem` int(11) NOT NULL,
  `lengthItem` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `imageItem` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`idItem`),
  KEY `idTypeItemTI_idx` (`idTypeItem`),
  CONSTRAINT `idTypeItemTI` FOREIGN KEY (`idTypeItem`) REFERENCES `TypeItem` (`idTypeItem`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items`
--

LOCK TABLES `Items` WRITE;
/*!40000 ALTER TABLE `Items` DISABLE KEYS */;
INSERT INTO `Items` VALUES (1,'Полотенце кухонное №П02-3',26,1,30,25,'Состав: 100% хлопок.','https://jysk.by/media/catalog/product/cache/image/beff4985b56e3afdbeabfc89641a4582/4/0/403f570a7ee0bcfcce228646f523d5e5bb546991___1_528-176-1002_2_.jpg'),(2,'Полотенце махровое кухонное №3030-17',35,1,30,70,'хлопок.','http://mamahealth.ru/wp-content/uploads/2012/01/2.jpg'),(3,'Одеяло №0932',500,3,140,205,'Наполнитель: синтепон (синтетический материал, обладающий легкостью, высокими теплозащитными свойствами), ПЭ.','http://cdn.grandstock.ru/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/7/1/7166ba30_4d5d_11e4_9627_001fd028fc3b_bfe8dd42_501c_11e4_9579_3c970e47a3e3_2.jpeg'),(4,'Подушка №0961',250,2,50,70,'Наполнитель: бамбук (Волокно бамбука принимает любую форму, не впитает посторонних запахов, обладает пылеотталкивающими свойствами), полиэстер.','https://tekstilka.ru/wp-content/uploads/2016/08/201404181549445831.jpg');
/*!40000 ALTER TABLE `Items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Job`
--

DROP TABLE IF EXISTS `Job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Job` (
  `idJob` int(11) NOT NULL AUTO_INCREMENT,
  `nameJob` varchar(45) NOT NULL,
  `salaryJob` int(11) NOT NULL,
  PRIMARY KEY (`idJob`),
  UNIQUE KEY `nameJob_UNIQUE` (`nameJob`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Job`
--

LOCK TABLES `Job` WRITE;
/*!40000 ALTER TABLE `Job` DISABLE KEYS */;
INSERT INTO `Job` VALUES (1,'Курьер',25000);
/*!40000 ALTER TABLE `Job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TypeItem`
--

DROP TABLE IF EXISTS `TypeItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TypeItem` (
  `idTypeItem` int(11) NOT NULL AUTO_INCREMENT,
  `nameTypeItem` varchar(45) NOT NULL,
  PRIMARY KEY (`idTypeItem`),
  UNIQUE KEY `TypeItemcol_UNIQUE` (`nameTypeItem`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TypeItem`
--

LOCK TABLES `TypeItem` WRITE;
/*!40000 ALTER TABLE `TypeItem` DISABLE KEYS */;
INSERT INTO `TypeItem` VALUES (3,'Одеяла'),(2,'Подушки'),(1,'Полотенца для кухни');
/*!40000 ALTER TABLE `TypeItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Warehouse`
--

DROP TABLE IF EXISTS `Warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Warehouse` (
  `idWarehouse` int(11) NOT NULL AUTO_INCREMENT,
  `addresWarehouse` varchar(100) NOT NULL,
  PRIMARY KEY (`idWarehouse`),
  UNIQUE KEY `addresWarehouse_UNIQUE` (`addresWarehouse`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Warehouse`
--

LOCK TABLES `Warehouse` WRITE;
/*!40000 ALTER TABLE `Warehouse` DISABLE KEYS */;
INSERT INTO `Warehouse` VALUES (1,'1');
/*!40000 ALTER TABLE `Warehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Warehouse_has_Items`
--

DROP TABLE IF EXISTS `Warehouse_has_Items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Warehouse_has_Items` (
  `idWarehouse` int(11) NOT NULL,
  `idItem` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`idWarehouse`,`idItem`),
  KEY `fk_Warehouse_has_Items_Items1_idx` (`idItem`),
  KEY `fk_Warehouse_has_Items_Warehouse1_idx` (`idWarehouse`),
  CONSTRAINT `idItemWI` FOREIGN KEY (`idItem`) REFERENCES `Items` (`idItem`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idWarehouseWI` FOREIGN KEY (`idWarehouse`) REFERENCES `Warehouse` (`idWarehouse`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Warehouse_has_Items`
--

LOCK TABLES `Warehouse_has_Items` WRITE;
/*!40000 ALTER TABLE `Warehouse_has_Items` DISABLE KEYS */;
/*!40000 ALTER TABLE `Warehouse_has_Items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Workers`
--

DROP TABLE IF EXISTS `Workers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Workers` (
  `idWorker` int(11) NOT NULL AUTO_INCREMENT,
  `LNameW` varchar(45) NOT NULL,
  `FNameW` varchar(45) NOT NULL,
  `PNameW` varchar(45) DEFAULT NULL,
  `phoneW` varchar(18) DEFAULT NULL,
  `passportW` varchar(12) NOT NULL,
  `idJob` int(11) NOT NULL,
  `idWarehouse` int(11) NOT NULL,
  PRIMARY KEY (`idWorker`),
  UNIQUE KEY `passportW_UNIQUE` (`passportW`),
  UNIQUE KEY `phoneW_UNIQUE` (`phoneW`),
  KEY `idJob_idx` (`idJob`),
  KEY `idWarehouse_idx` (`idWarehouse`),
  CONSTRAINT `idJob` FOREIGN KEY (`idJob`) REFERENCES `Job` (`idJob`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idWarehouse` FOREIGN KEY (`idWarehouse`) REFERENCES `Warehouse` (`idWarehouse`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Workers`
--

LOCK TABLES `Workers` WRITE;
/*!40000 ALTER TABLE `Workers` DISABLE KEYS */;
INSERT INTO `Workers` VALUES (1,'1','1','1','1','1',1,1);
/*!40000 ALTER TABLE `Workers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typeAccount`
--

DROP TABLE IF EXISTS `typeAccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typeAccount` (
  `idTypeAccount` int(11) NOT NULL AUTO_INCREMENT,
  `nameType` varchar(30) NOT NULL,
  PRIMARY KEY (`idTypeAccount`),
  UNIQUE KEY `nameType_UNIQUE` (`nameType`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typeAccount`
--

LOCK TABLES `typeAccount` WRITE;
/*!40000 ALTER TABLE `typeAccount` DISABLE KEYS */;
INSERT INTO `typeAccount` VALUES (2,'Администратор'),(1,'Пользователь');
/*!40000 ALTER TABLE `typeAccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'Tekstil'
--
/*!50003 DROP PROCEDURE IF EXISTS `AddContract` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `AddContract`(IN idClientIN INT, IN idCarIN INT, IN addresContractIN VARCHAR(100), 
IN dateContractIN DATE)
BEGIN
	INSERT INTO Contract (idClient, idCar, addresContract, dateContract) VALUES
    (idClientIN, idCarIN, addresContractIN, dateContractIN);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddContractHasItems` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `AddContractHasItems`(IN idContractIN INT, IN idItemIN INT, IN countIN INT, in itemID int)
BEGIN
	INSERT INTO Contract_has_Items (idContract, idItem, `count`) VALUES
    (idContractIN, idItemIN, countIN);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddContractHasItems_v2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `AddContractHasItems_v2`(IN idClientIN INT, IN idCarIN INT, IN addresContractIN VARCHAR(100), 
IN dateContractIN DATE, IN idItemIN INT, IN countIN INT)
BEGIN
	INSERT INTO Contract (idClient, idCar, addresContract, dateContract) VALUES
    (idClientIN, idCarIN, addresContractIN, dateContractIN);
	INSERT INTO Contract_has_Items (idContract, idItem, `count`) VALUES
    (last_insert_id(), idItemIN, countIN);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CheckAcc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `CheckAcc`(IN loginIn VARCHAR(45), OUT resultOut VARCHAR(45))
BEGIN
	SET resultOut = IFNULL(
		(
			SELECT 1
			FROM Accounts 
			WHERE login = TRIM(loginIn)
			LIMIT 1
		), 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CheckAccLogIn` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `CheckAccLogIn`(IN loginIn VARCHAR(45), IN passIn VARCHAR(45), OUT loginOut VARCHAR(45))
BEGIN
	SET loginOut = IFNULL(
		(
			SELECT 1
			FROM Accounts 
			WHERE login = TRIM(loginIn) AND `password` = TRIM(passIn)
			LIMIT 1
		), 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CheckMail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `CheckMail`(IN emailIn VARCHAR(32), OUT resultOut VARCHAR(45))
BEGIN
	SET resultOut = IFNULL(
		(
			SELECT 1
			FROM Accounts 
			WHERE email = TRIM(emailIn)
			LIMIT 1
		), 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CheckNumb` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `CheckNumb`(IN numbIn VARCHAR(18), OUT resultOut VARCHAR(45))
BEGIN
	SET resultOut = IFNULL(
		(
			SELECT 1
			FROM Clients 
			WHERE PhoneC = TRIM(numbIn)
			LIMIT 1
		), 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetAccountID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetAccountID`(IN loginIn VARCHAR(60), OUT IDOut VARCHAR(5))
BEGIN
	SET IDOut = 
		(
			SELECT idAcc
			FROM Accounts
			WHERE login = TRIM(loginIn)
			LIMIT 1
		);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetAllCars` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetAllCars`(OUT carOUT VARCHAR(10000))
BEGIN
	SET carOUT = 
    (
		SELECT GROUP_CONCAT(CONCAT(carName, ' ', carModel, '-', regNum))
        FROM Cars
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetAllCarsNames` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetAllCarsNames`(OUT carOUT VARCHAR(10000))
BEGIN
	SET carOUT = 
    (
		SELECT GROUP_CONCAT(CONCAT(carName, ' ', carModel))
        FROM Cars
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetAllItems` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetAllItems`(OUT idItemOUT VARCHAR(10000), OUT itemsOUT VARCHAR(10000))
BEGIN
	SET idItemOUT =
    (
		SELECT GROUP_CONCAT(idItem)
		FROM Items
        ORDER BY idItem ASC
    );

	SET itemsOUT =
    (
		SELECT GROUP_CONCAT(itemName)
		FROM Items
        ORDER BY idItem ASC
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetAllItemsOnName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetAllItemsOnName`(IN nameIN VARCHAR(45), OUT idItemOUT VARCHAR(10000), OUT itemsOUT VARCHAR(10000))
BEGIN
	SET idItemOUT =
    (
		SELECT GROUP_CONCAT(idItem)
		FROM Items
        WHERE REPLACE(itemName, '-', '') LIKE CONCAT('%', TRIM(REPLACE(nameIN, '-', '')), '%')
        ORDER BY idItem ASC
    );

	SET itemsOUT =
    (
		SELECT GROUP_CONCAT(itemName)
		FROM Items
        WHERE REPLACE(itemName, '-', '') LIKE CONCAT('%', TRIM(REPLACE(nameIN, '-', '')), '%')
        ORDER BY idItem ASC
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetCarID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetCarID`(IN regNumIN VARCHAR(10), OUT idCarOUT INT)
BEGIN
	SET idCarOUT =
    (
		SELECT idCar
        FROM Cars
        WHERE regNum = TRIM(regNumIN)
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetClientID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetClientID`(IN loginIn VARCHAR(45), IN passIn VARCHAR(45), OUT IDOut VARCHAR(45))
BEGIN
	SET IDOut = 
		(
			SELECT idClient
			FROM Clients JOIN Accounts ON Clients.idAcc = Accounts.idAcc
			WHERE login = TRIM(loginIn) AND `password` = TRIM(passIn)
			LIMIT 1
		);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetContractOnStatus` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetContractOnStatus`(in statusContractIn tinyint(1), in clientIdIn int, out contractIdOut VARCHAR(10000), out carIdOut VARCHAR(10000), out addrOut VARCHAR(10000), out dateContr VARCHAR(10000))
BEGIN
	set contractIdOut = (select GROUP_CONCAT(idContract) from Contract where statusContract = statusContractIn and idClient = clientIdIn order by idContract asc);
    set addrOut = (select GROUP_CONCAT(addresContract) from Contract where statusContract = statusContractIn and idClient = clientIdIn order by idContract asc);
    set carIdOut = (select GROUP_CONCAT(idCar) from Contract where statusContract = statusContractIn  and idClient = clientIdIn order by idContract asc);
    set dateContr = (select GROUP_CONCAT(dateContract) from Contract where statusContract = statusContractIn and idClient = clientIdIn order by idContract asc);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetImage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetImage`(IN idItemIN INT, OUT photoOUT VARCHAR(1024))
BEGIN
	SET photoOUT = (
		SELECT imageItem
		FROM Items
		WHERE idItem = TRIM(idItemIN)
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetItemInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetItemInfo`(IN itemNameIn VARCHAR(45), OUT itemNameOUT VARCHAR(45), OUT nameProducerOUT VARCHAR(45),
OUT priceItemOUT INT, OUT nameTypeItemOUT VARCHAR(45), OUT widthItemOUT INT, OUT lengthItemOUT INT, 
OUT noteOUT VARCHAR(255), OUT photoOUT VARCHAR(1024))
BEGIN
    SET itemNameOUT = IFNULL(
    (
		SELECT itemName
		FROM Items
		WHERE itemName = TRIM(itemNameIn)
    ), 0);
    
    SET nameProducerOUT = IFNULL(
    (
		SELECT Producer.nameProducer
		FROM Items JOIN Producer ON Items.idItem = Producer.idProducer
		WHERE itemName = TRIM(itemNameIn)
    ), 0);
    
    SET priceItemOUT = IFNULL( 
    (
		SELECT priceItem
		FROM Items
		WHERE itemName = TRIM(itemNameIn)
    ), 0);
    
    SET nameTypeItemOUT = IFNULL(
    (
		SELECT TypeItem.nameTypeItem
		FROM Items JOIN TypeItem ON Items.idTypeItem = TypeItem.idTypeItem
		WHERE itemName = TRIM(itemNameIn)
    ), 0);
    
    SET widthItemOUT = IFNULL(
    (
		SELECT widthItem
		FROM Items
		WHERE itemName = TRIM(itemNameIn)
    ), 0);
    
    SET lengthItemOUT = IFNULL(
    (
		SELECT lengthItem
		FROM Items
		WHERE itemName = TRIM(itemNameIn)
    ), 0);
    
    SET noteOUT = IFNULL(
    (
		SELECT note
		FROM Items
		WHERE itemName = TRIM(itemNameIn)
    ), 0);
    
    SET photoOUT = IFNULL(
    (
		SELECT imageItem
		FROM Items
		WHERE itemName = TRIM(itemNameIn)
    ), 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetItemInfoID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetItemInfoID`(IN itemIdIN INT, OUT itemNameOUT VARCHAR(100),
OUT priceItemOUT INT, OUT nameTypeItemOUT VARCHAR(45), OUT widthItemOUT INT, OUT lengthItemOUT INT, 
OUT noteOUT VARCHAR(255), OUT photoOUT VARCHAR(1024))
BEGIN
	SET itemNameOUT = IFNULL(
    (
		SELECT itemName
		FROM Items
		WHERE idItem = TRIM(itemIdIN)
        ORDER BY idItem ASC
    ), 0);
    
    SET priceItemOUT = IFNULL( 
    (
		SELECT priceItem
		FROM Items
		WHERE idItem = TRIM(itemIdIN)
        ORDER BY idItem ASC
    ), 0);
    
    SET nameTypeItemOUT = IFNULL(
    (
		SELECT TypeItem.nameTypeItem
		FROM Items JOIN TypeItem ON Items.idTypeItem = TypeItem.idTypeItem
		WHERE idItem = TRIM(itemIdIN)
        ORDER BY idItem ASC
    ), 0);
    
    SET widthItemOUT = IFNULL(
    (
		SELECT widthItem
		FROM Items
		WHERE idItem = TRIM(itemIdIN)
        ORDER BY idItem ASC
    ), 0);
    
    SET lengthItemOUT = IFNULL(
    (
		SELECT lengthItem
		FROM Items
		WHERE idItem = TRIM(itemIdIN)
        ORDER BY idItem ASC
    ), 0);
    
    SET noteOUT = IFNULL(
    (
		SELECT note
		FROM Items
		WHERE idItem = TRIM(itemIdIN)
        ORDER BY idItem ASC
    ), 0);
    
    SET photoOUT = IFNULL(
    (
		SELECT imageItem
		FROM Items
		WHERE idItem = TRIM(itemIdIN)
        ORDER BY idItem ASC
    ), 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getItemPrice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `getItemPrice`(OUT idItemOUT VARCHAR(10000), OUT itemsOUT VARCHAR(10000), OUT priceOUT VARCHAR(1000))
BEGIN
	SET idItemOUT =
    (
		SELECT GROUP_CONCAT(idItem)
		FROM Items
    );

	SET itemsOUT =
    (
		SELECT GROUP_CONCAT(itemName)
		FROM Items
    );
    
    SET priceOUT =
    (
		SELECT GROUP_CONCAT(priceItem)
        FROM Items
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetLastContractID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetLastContractID`(OUT idContractOUT VARCHAR(5))
BEGIN
	SET idContractOUT =
    (
		SELECT MAX(idContract)
        FROM Contract
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetOrderItems` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetOrderItems`(IN idContractIN INT, OUT itemIdOUT VARCHAR(10000), OUT itemNameOUT VARCHAR(10000),
OUT countOUT VARCHAR(10000), OUT priceOUT VARCHAR(10000), OUT statusOUT VARCHAR(5))
BEGIN
	SET itemIdOUT = (
		SELECT GROUP_CONCAT(idItem)
		FROM Contract_has_Items
		WHERE Contract_has_Items.idContract = idContractIN
    );
    
    SET itemNameOUT = (
		SELECT GROUP_CONCAT(Items.itemName)
		FROM Contract_has_Items JOIN Items ON Contract_has_Items.idItem = Items.idItem
		WHERE Contract_has_Items.idContract = idContractIN
    );
    
    SET countOUT = (
		SELECT GROUP_CONCAT(`count`)
		FROM Contract_has_Items
		WHERE Contract_has_Items.idContract = idContractIN
    );
    
    SET priceOUT = IFNULL((
		SELECT SUM(Items.priceItem * `count`)
		FROM Contract_has_Items JOIN Items ON Contract_has_Items.idItem = Items.idItem
		WHERE Contract_has_Items.idContract = idContractIN
    ), 0);
    
    SET statusOUT = (
		SELECT statusContract
		FROM Contract
		WHERE idContract = idContractIN
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetPhoto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `GetPhoto`(IN idItemIN INT, OUT photoOUT LONGBLOB)
BEGIN
	SET photoOUT = (
		SELECT imageItem
		FROM Items
		WHERE idItem = TRIM(idItemIN)
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `RegisterAcc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `RegisterAcc`(IN loginIn VARCHAR(45), IN passwordIn VARCHAR(45), IN emailIn VARCHAR(32))
BEGIN
	INSERT INTO Accounts (login, `password`, email, idTypeAccount) VALUES (loginIn, passwordIn, emailIn, 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `RegisterClient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `RegisterClient`(IN lNameIn VARCHAR(32), IN fNameIn VARCHAR(45), 
IN pNameIn VARCHAR(45), IN idAccIN INT, IN numbIn VARCHAR(18))
BEGIN
	INSERT INTO Clients (LNameC, FNameC, PNameC, idAcc, PhoneC) VALUES (lNameIn, fNameIn, pNameIn, idAccIN, numbIn);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `RemoveContract` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `RemoveContract`(in contractID INT)
BEGIN
	DELETE FROM Contract WHERE idContract = contractID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TEST_Get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`student`@`%` PROCEDURE `TEST_Get`(IN idContractIN INT, IN kilometersIN INT, OUT idCarOUT VARCHAR(10),
OUT priceOUT VARCHAR(10))
BEGIN
	SET idCarOUT = IFNULL(
    (
		SELECT Cars.idCar
		FROM Cars
		WHERE Cars.widthBody * Cars.heightBody * Cars.lengthBody >=
		(
			SELECT SUM(MyItems.v) AS Volume
			FROM
			(
				SELECT Contract_has_Items.idItem, Contract_has_Items.`count`, (Items.widthItem * Items.heightItem * Items.lengthItem) * Contract_has_Items.`count` AS v
				FROM Contract_has_Items JOIN Items ON Contract_has_Items.idItem = Items.idItem
				WHERE idContract = idContractIN
			) AS MyItems
		)
		AND Cars.state = 0
		LIMIT 1
    ), 'Нет машин');
    
    SET priceOUT =
    (
		SELECT SUM(ItemsPR.pr) + (kilometersIN * 100)
		FROM
		(
			SELECT Items.priceItem * Contract_has_Items.`count` AS pr
			FROM Contract_has_Items JOIN Items ON Contract_has_Items.idItem = Items.idItem
			WHERE Contract_has_Items.idContract = 19
		) AS ItemsPR
    );
    
	UPDATE Cars
	SET state = 1
	WHERE idCar = idCarOUT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `СompletedOrders`
--

/*!50001 DROP VIEW IF EXISTS `СompletedOrders`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`student`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `СompletedOrders` AS select `Contract`.`idContract` AS `idContract`,`Contract`.`idClient` AS `idClient`,`Contract`.`idCar` AS `idCar`,`Contract`.`addresContract` AS `addresContract`,`Contract`.`dateContract` AS `dateContract` from `Contract` where (`Contract`.`statusContract` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-12 13:04:06
